package br.com.itau.CacaNiquel;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Assert;
import org.junit.Before;

public class CacaNiquelTest {
	CacaNiquel cacaNiquel;

	@Before
	public void preparar() throws Exception {
		cacaNiquel = new CacaNiquel();
	}

	@Test
	public void criarCacaNiquelCorretamente() {
		assertNotNull(cacaNiquel);
		assertNotNull(cacaNiquel.getFiguras());
		assertEquals(cacaNiquel.getFiguras().size(), 3);
	}

	@Test
	public void jogar() {

//		cacaNiquel.jogar();
	}
}
