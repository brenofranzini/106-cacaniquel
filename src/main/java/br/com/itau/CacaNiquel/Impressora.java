package br.com.itau.CacaNiquel;

import java.util.List;

public class Impressora {
	public static void imprimePontuacao(String saida) {
		System.out.println();
		System.out.println("Jogador X");
		System.out.println("Sua pontuacao final foi de: " + saida);
	}

	public static void imprimeFiguras(List<String> figuras) {
		String separador = ", ";		
		String fig = "[";

		for (String figura : figuras)
			fig += figura + separador;

		fig = fig.substring(0, fig.length() - 2);
		fig += "]";
		System.out.println(fig);
	}
}
