package br.com.itau.CacaNiquel;

public enum Figura {
	PERA(10),
	MACA(20),
	JABOTICABA(50),
	SETE(100);
	
	private Figura(int peso) {
		this.peso = peso;
	}
	
	private int peso;
	
	public int getPeso() {
		return peso;
	}
}
