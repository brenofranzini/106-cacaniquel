package br.com.itau.CacaNiquel;

import java.util.ArrayList;
import java.util.List;

public class CacaNiquel {

	private List<Figura> figuras;

	public CacaNiquel() {

		figuras = Sorteador.sortearFigura();
		
		int valor = Calculador.calcularPontuacao(this);
		String pontuacao = Integer.toString(valor);
		
		Impressora.imprimePontuacao(pontuacao);
		
		
		List<String> lista = new ArrayList<String>();
		for(Figura figura : figuras)
			lista.add(figura.name());
		Impressora.imprimeFiguras(lista);
	}

	public List<Figura> getFiguras() {
		return figuras;
	}
}
