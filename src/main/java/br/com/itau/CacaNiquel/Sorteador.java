package br.com.itau.CacaNiquel;

import java.util.ArrayList;
import java.util.List;

public class Sorteador {

	private static List<Figura> figuraProbabilidade;
	public static List<Figura> figuras;

	public static List<Figura> sortearFigura() {
		figuras = new ArrayList<Figura>();
		figuraProbabilidade = new ArrayList<Figura>();

		int numeroSorteado = 0;
		inicializaListaSorteioProbabilistico();

		for (int i = 0; i < 3; i++) {
			numeroSorteado = sortearPosicao();
			figuras.add(figuraProbabilidade.get(numeroSorteado));
		}

		return figuras;
	}

	private static int sortearPosicao() {

		int numeroElementos = figuraProbabilidade.size();

		return (int) (Math.floor(Math.random() * numeroElementos));
	}

	private static void inicializaListaSorteioProbabilistico() {
		int percentual7 = 1;
		int demaisFiguras = 3;

		for (int i = 0; i < percentual7; i++)
			figuraProbabilidade.add(Figura.SETE);
		
		for (Figura f : Figura.values()) {
			if(f.name() != Figura.SETE.name()) {
				for (int i = 0; i < demaisFiguras; i++)
					figuraProbabilidade.add(f);	
			}
		}
	}
}
