package br.com.itau.CacaNiquel;

public class Calculador {

	public static int calcularPontuacao(CacaNiquel cacaNiquel) {

		if (pontuacaoMaxima(cacaNiquel)) {
			int peso = Figura.SETE.getPeso();
			int quantidadeFiguras = cacaNiquel.getFiguras().size();

			return peso * quantidadeFiguras * 5;
		}

		int pontuacao = 0;

		for (Figura figura : cacaNiquel.getFiguras())
			pontuacao += figura.getPeso();

		if (pontuacao3X(cacaNiquel)) {
			pontuacao *= 3;
			return pontuacao;
		}

		if (pontuacao2X(cacaNiquel)) {
			pontuacao *= 2;
			return pontuacao;
		}

		return pontuacao;

	}

	private static boolean pontuacaoMaxima(CacaNiquel cacaNiquel) {
		for (Figura figura : cacaNiquel.getFiguras()) {
			if (Figura.SETE.name() != figura.name())
				return false;
		}
		return true;
	}

	private static boolean pontuacao3X(CacaNiquel cacaNiquel) {
		String figura1 = cacaNiquel.getFiguras().get(0).name();
		String figura2 = cacaNiquel.getFiguras().get(1).name();
		String figura3 = cacaNiquel.getFiguras().get(2).name();
		
		return figura1.equals(figura2) && figura1.equals(figura3);
	}

	private static boolean pontuacao2X(CacaNiquel cacaNiquel) {
		String figura1 = cacaNiquel.getFiguras().get(0).name();
		String figura2 = cacaNiquel.getFiguras().get(1).name();
		String figura3 = cacaNiquel.getFiguras().get(2).name();
		
		return figura1.equals(figura2) ||
				figura1.equals(figura3) ||
				figura2.equals(figura3);
	}
}
