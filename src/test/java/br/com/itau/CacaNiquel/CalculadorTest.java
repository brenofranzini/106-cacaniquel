package br.com.itau.CacaNiquel;

import org.junit.Before;
import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;

public class CalculadorTest {
	CacaNiquel cacaNiquel;

	@Before
	public void preparar() throws Exception {
		cacaNiquel = new CacaNiquel();
		Calculador c = new Calculador();
	}

	@Test
	public void calcularPontuacao() {
		int pontuacao = Calculador.calcularPontuacao(cacaNiquel);
		assertTrue(pontuacao > 0);
	}
	
	@Test(expected=AssertionError.class)
	public void calcularPontuacaoError() {
		int pontuacao = Calculador.calcularPontuacao(cacaNiquel);
		assertTrue(pontuacao <= 0);
	}
}
